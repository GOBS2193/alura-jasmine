describe('Testa função maior menor', function () {
    var maiorEMenor;

    beforeEach(function () {
        maiorEMenor = new MaiorEMenor();
    });

    it('should return the biggest and the smaller number', function () {

        maiorEMenor.encontra([1,2,3,4,5,6])
        expect(maiorEMenor.pegaMenor()).toEqual(1);
        expect(maiorEMenor.pegaMaior()).toEqual(6);
    })

    it("deve entender numeros em ordem nao sequencial", function() {
        var algoritmo = new MaiorEMenor();
        algoritmo.encontra([5,15,7,9]);

        expect(algoritmo.pegaMaior()).toEqual(15);
        expect(algoritmo.pegaMenor()).toEqual(5);
    });

    it('should find number in downward order', function() {
        maiorEMenor.encontra([6,5,4,3,2,1])
        expect(maiorEMenor.pegaMenor()).toEqual(1);
        expect(maiorEMenor.pegaMaior()).toEqual(6);
    })

    it('should work in a list of one element', function() {
        maiorEMenor.encontra([5]);
        expect(maiorEMenor.pegaMenor()).toEqual(5);
        expect(maiorEMenor.pegaMaior()).toEqual(5);
    })

})