describe('Testa Paciente', function(){
    var paciente;

    it('should calculate the IMC', function () {
        paciente = new Paciente ('Gabriel', 1.70, 70, 24)
        expect(paciente.imc()).toEqual(70/(1.70*1.70))
    })

    it ('Should test the heartbeats', function () {
        paciente = new Paciente('Gabriel', 1.70, 70, 24)
        expect(paciente.batimentos()).toEqual(24*365*24*60*80);
    })
})