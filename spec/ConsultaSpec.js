describe('Testa consulta', function () {
    var paciente;
    var consulta;

    beforeEach(function () {
        paciente = new PacienteBuilder().constroi();
    });

    describe('Consulta com retorno', function () {
        it('Deve testar o retorno', function () {
            consulta = new Consulta(paciente,[],true,true);
            expect(consulta.preco()).toEqual(0);
        });
    });

    describe('Consulta normal', function () {
        it('Deve testar um procedimento normal', function() {
            consulta = new Consulta(paciente,['p1','p2','p3'],false,false);
            expect(consulta.preco()).toEqual(75);
        });
    });

    describe('consulta particular', function () {
        it('Deve testar um procedimento particular', function() {
            consulta = new Consulta(paciente,['p1'],true,false);
            expect(consulta.preco()).toEqual(50);
        });

        it('Deve testar um procedimento particular especial', function() {
            consulta = new Consulta(paciente,['p1','gesso'],true,false);
            expect(consulta.preco()).toEqual((25+32)*2);
        });
    })
});